#!/bin/bash
set -e
cd ../source

echo "---Start autogen.sh---"
NOCONFIGURE=1 ./autogen.sh
echo "---End autogen.sh---"
echo "--------------------"

./configure --disable-static --with-graphite2 --with-gobject --enable-introspection
echo "--------------------"
echo "---Start make check---"
make check
retval=$?
echo $retval
if [ $retval -ne 0 ]; then
        echo "make check failed"
else
        echo "make check completed sucessfully"
fi
echo "---End make check---"
